#!/usr/bin/env python3
from bs4 import BeautifulSoup
import requests
import logging
import argparse


def parse_catalog(url):
    counter = 1
    listed_products = []
    paged_url = url[:-4] + '-p{0}' + url[-4:]  # Should be indexing the last '.' or using regexps
    more_results = True

    while more_results:
        if counter == 1:
            logging.info("Parsing page: {0}".format(counter))  # Just to keep track of the progress
            page = requests.get(url)
            counter += 1
        else:
            logging.info("Parsing page: {0}".format(counter))  # Just to keep track of the progress
            page = requests.get(paged_url.format(counter))
            counter += 1
        soup = BeautifulSoup(page.content, 'html.parser')
        more_results = soup.find(class_='next fa fa-chevron-right')  # There's a 'next page' on every page except the last one
        results = soup.find_all(class_='name browsinglink')
        for elem in results:
            listed_products.append(elem.string)
    return listed_products


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Scrapper for Alza's smart section catalog")
    parser.add_argument("-u", "--url", help="absolute URL to catalog section "
                                            "(i.e: https://www.alza.cz/trendy/chytre-hodinky-smartwatch/18854785.htm")
    parser.add_argument("-v", "--verbose", help="log page being processed", action="store_true")
    args = parser.parse_args()
    if args.verbose:
        logging.basicConfig(level=logging.INFO)
    if args.url:
        results = parse_catalog(args.url)
        logging.info("-+--+-+-+-+-+-+-+-+-")
        print("There's a total of {0} units:".format(len(results)))
        for elem in results:
            print(elem)
    else:
        parser.print_help()
