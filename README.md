### Requirements ###
* `python >= 3`
* `pip`
* `virtualenv`

### How do I get set up? ###

The following steps show how to work with the project using a virtual environment (this really is the way to go):

* `$ git clone https://Av4t4r@bitbucket.org/Av4t4r/scrapper.git` 
* `$ cd scrapper`
* `$ virtualenv venv`
* `$ source venv/bin/activate`
* `$ pip install -r requirements.txt`
* `$ python main.py`
* Follow on-screen instructions
